<link rel="stylesheet" data-enableoptimizations="true" href="@App.Path/Portals/_default/Skins/MedmenCorporate_2018/css/global.css" />

@using Connect.Koi;

@{
    var lib = CreateInstance("_library.cshtml"); 

    var catFilter = Request.QueryString["category"];
    var hasCategoryFilter = !String.IsNullOrEmpty(catFilter);
    var allCategories = AsDynamic(App.Data["Category"]);

    var newsPage = @Dnn.Tab.FullUrl;
    var newsPageTab = @Dnn.Module.TabID;
}
@if(!String.IsNullOrEmpty(Content.NewsPage)) 
{
    newsPage = Content.NewsPage;
    newsPageTab = int.Parse((AsEntity(Content).GetBestValue("NewsPage")).Split(':')[1]);
}

<div class="app-news app-news-list sc-element @((Permissions.UserMayEditContent) ? "app-edit" : "")" >
    @Edit.Toolbar(actions: "new, contentitems", contentType: "News", prefill: new { Date = DateTime.Now.Date, PublicationMoment = DateTime.Now.Date, DeprecationMoment =  (DateTime.Now.AddYears(3)).Date } , settings: new { hover = "left", showCondition = true })
    @* @Edit.Toolbar(Content) *@
    
    
    <!--Category Filter == Displayed only if a category has been selected -->
        @if (hasCategoryFilter) {
            <div class="app-catinfo">
                <h2>@allCategories.Where(x => x.UrlKey == catFilter).FirstOrDefault().Name</h2>
                @foreach (var c in allCategories)
                {
                    <span><a @Koi.Class("all='" + (Request.QueryString["category"] == c.UrlKey ? "active" : "") + "' bs3='btn btn-primary btn-xs' bs4,unk='btn btn-primary btn-sm'") href="@lib.LinkToPage(newsPageTab, "category", c.UrlKey, "", "", "", "")">@c.Name</a></span>
                }
            </div>
        }
    <div class="c-newsroom-listing">
        
        <!-- Beginning of content loop -->
        @foreach (var item in @AsDynamic(Data["News"]))
        {
        
        <div class="c-newsroom-listing__item c-newsroom-listing__item--wide wow fadeInUp")>
            
            
            
            
            
            
            @{
                string detailsLink = @lib.LinkToPage(newsPageTab, "mid", ((String.IsNullOrEmpty(Content.MainModuleId)) ? (Dnn.Module.ModuleID).ToString() : (Content.MainModuleId)), "details", item.EntityId.ToString(), "title", item.UrlKey);
            }
            <div class="app-details-link" onclick="window.open('@detailsLink','_self');">
                <div class="row">
                    <div @Koi.Class("bs3='col-xs-12 col-sm-6 col-md-4' bs4,unk='col-12 col-md-6 col-lg-4'")>
                    <!-- Image or Placeholder -->
                        <div class="app-img @((String.IsNullOrEmpty(@item.Image)) ? "app-noimg" : "")">
                            <img @Koi.Class("bs3='img-responsive' bs4,unk='img-fluid'") src="@((!String.IsNullOrEmpty(@item.Image)) ? @item.Image + "?w=800&h=480&mode=crop&scale=both&quality=80&anchor=" + @item.CropAnchor : @App.Resources.PlaceholderMissingImage + "?max-height=210&mode=max&scale=both&quality=80")"alt="@item.Title" />
                        </div>
                    </div>
                
                    <div @Koi.Class("bs3='col-xs-12 col-sm-6 col-md-8' bs4,unk='col-12 col-md-6 col-lg-8'")>
                    <!-- Text Date Categories -->
                        <div class="app-text">
                            <h2 class="app-list">@item.Title</h2>
                        
                            <span class="app-date @((item.Categories.Count >  0 && App.Settings.ActivateCategories) ? "app-date-border" : "")">@item.Date.ToString("dd.MM.yyyy")</span>
                            <span class="app-categories">
                            @if(App.Settings.ActivateCategories) {
                                foreach (var cat in  @item.Categories)
                                {
                                    <a href="@lib.LinkToPage(newsPageTab, "category", cat.UrlKey, "", "", "", "")">@cat.Name</a>
                                }
                            }
                            </span>
                        
                            <p>
                                @Html.Raw(item.Teaser.Replace("\n", "<br />"))
                            </p>
                        </div>
                        
                        <span class="app-readmore">@Html.Raw(App.Resources.LabelReadMore)</span>
                    </div>
                </div>
            </div>                    
        </div>
        } <!-- END of content loop -->

    </div>
    @if (hasCategoryFilter) {
        <a @Koi.Class("all='app-backtolist' bs3='btn btn-primary' bs4,unk='btn btn-outline-primary'") href="@newsPage">@Html.Raw(App.Resources.LabelBackToList)</a>
    }

    @if (Content.ShowPagination) {
        @RenderPage("_paging.cshtml", new { newsPage = newsPage }) 
    }
</div>

<link rel="stylesheet" href="@App.Path/dist/@(Koi.PickCss("bs3,bs4", "bs3")).css" data-enableoptimizations="true" />
<script src="@App.Path/dist/script.js" data-enableoptimizations="true" ></script>
