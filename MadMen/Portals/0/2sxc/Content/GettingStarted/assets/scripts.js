$(document).ready(function() {
	
	/* Youtube video template */
	$('.co-video-youtube .co-video-wrapper').each(function() {
		var videoLink = $(this).attr('data-videolink');
		var videoId = GetYoutubeIdFromUrl(videoLink);
		if(!videoId)
			$(this).append('<div class="dnnFormMessage dnnFormWarning">The youtube video id was not found. Please make sure you entered a correct youtube video link.</div>');
		else
			$(this).append($('<iframe></iframe>').attr('src', '//www.youtube.com/embed/' + videoId + '?wmode=transparent'));
	});
	
	/* Vimeo video template */
	$('.co-video-vimeo .co-video-wrapper').each(function() {
		var videoLink = $(this).attr('data-videolink');
		var videoId = GetVimeoIdFromUrl(videoLink);
		if(!videoId)
			$(this).append('<div class="dnnFormMessage dnnFormWarning">The vimeo video id was not found. Please make sure you entered a correct vimeo video link.</div>');
		else
			$(this).append($('<iframe></iframe>').attr('src', '//player.vimeo.com/video/' + videoId));
	});
	
	/* Image List Fancybox */
	$('.co-imagelist .co-imagelist-link').click(function(e) {
		e.preventDefault();
		var moduleId = $(this).attr('data-moduleid');
		var src = $(this).attr('href');
		$('#Image-' + moduleId).attr('src', src).load(function() {
			$('#Dialog-' + moduleId).dialog({
				dialogClass: 'dnnFormPopup co-imagelist-popup',
				modal: true,
				resizable: false,
				draggable: false,
				width: '990px'
			});
		});
	});
	
});


// This function searches for the youtube id in the url and returns it
function GetYoutubeIdFromUrl(url)
{
	var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = url.match(regExp);
	if (match&&match[2].length==11){
			return match[2];
	}else{
			return false;
	}
}

function GetVimeoIdFromUrl(url)
{
	var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
	var match = url.match(regExp);
	if (match){
			return match[2];
	}else{
			return false;
	}
}