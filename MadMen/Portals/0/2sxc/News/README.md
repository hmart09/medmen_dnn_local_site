## 2sxc News App

A standard news / announcements app to use with 2sxc

## Getting Started
This app is only useful is you use DNN. So assuming you have a DNN installation, all you need to do is install 2sxc and this app. 

* Here's how to [install 2sxc and an App of your Choice](http://2sxc.org/en/blog/post/install-2sxc-and-an-app-of-your-choice)

* Now you can use this app as-is, or customize it to be whatever you need it to be. 
