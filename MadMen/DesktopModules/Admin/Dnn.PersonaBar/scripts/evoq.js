'use strict';
define(['jquery', 'knockout', 'jquery.hoverIntent.min'], function ($, ko) {
    var mainFrame = window.parent.document.getElementById("personaBar-iframe");
    var personaBarWidth = 80;
    var util = null;

    var getSummaryContainer = function () {
        var $summary = $('.server-summary');
        if (!$summary.length) {
            $summary = $('' +
                '<div class="server-summary hoverSummaryMenu">' +
                    '<ul>' +
                        '<li class="title" data-bind="html: ProductName"></li>' +
                        '<li class="version-info" data-bind="html: ProductVersion"></li>' +
                        '<li class="framework" data-bind="visible: FrameworkVersion.length > 0"><span data-bind="html: FrameworkVersion"></span><label data-bind="html: resx.Framework"></label></li>' +
                        '<li class="server-name" data-bind="visible: ServerName.length > 0"><span data-bind="html: ServerName"></span><label data-bind="html: resx.ServerName"></label></li>' +
                        '<li class="license-info" data-bind="visible: License.length > 0"><span data-bind="html: License"></span><label data-bind="html: resx.LicenseKey"></label></li>' +
                        '<li data-bind="visible: FrameworkVersion.length == 0" class="separator"></li>' +
                        '<li class="doc-center"><a href="http://www.dnnsoftware.com/docs/" data-bind="html: resx.Documentation, visible: visibleCheck(\'DocCenterVisible\')" target="_blank"></a></li>' +
                        '<li class="customer-portal"><a href="http://www.dnnsoftware.com/services/customer-support/success-network" data-bind="html: resx.CustomerPortal, visible: visibleCheck(\'CustomerPortalVisible\')" target="_blank"></a></li>' +
                        '<li class="contact-support"><a data-bind="html: resx.ContactSupport, attr: {href: SupportMail}, visible: visibleCheck(\'ContactSupportVisible\')"></a></li>' +
                        '<li id="Logout" class="logout">Logout</li>' +
                    '</ul>' +
                '</div>');

            $('#personabar').find('.personabarLogo').append($summary);
        }

        return $summary;
    }


    var showServerSummary = function () {
        mainFrame.style.width = '100%';
        $('.hoverSummaryMenu, .hovermenu').hide();
        getSummaryContainer().addClass('shown');
    }

    var hideServerSummary = function () {
        if (!$('.hovermenu:visible').length && !$('.socialpanel:visible').length) {
            mainFrame.style.width = personaBarWidth + "px";
        }
        getSummaryContainer().removeClass('shown');
    }

    var getServerInfo = function(callback) {
        util.sf.moduleRoot = "personaBar";
        util.sf.controller = "evoq";
        util.sf.getsilence("GetServerInfo", {}, function (data) {
            if (typeof callback === "function") {
                callback(data);
            }
        });
    }

    var initialize = function () {
        if (!util.resx) {
            setTimeout(initialize, 500);
            return;
        }
        var $logo = $('#personabar').find('.personabarLogo');
        var $summaryContainer = getSummaryContainer();

        getServerInfo(function (info) {
            var viewModel = $.extend({}, info, { resx: util.resx.Evoq, visibleCheck: function(name) {
                return info[name];
            } });
            try {
                ko.applyBindings(viewModel, $summaryContainer[0]);
            } catch (ex) {
                
            }
        });
        
        $logo.hoverIntent({
            over: function() {
                showServerSummary();
            },
            out: function () {
                hideServerSummary();
            },
            timeout: 200
        });
    }

    return {
        init: function (utility) {
            util = utility;

            initialize();
        },
        load: function () {
            var self = this;
            var $tasks = $('.socialtasks');
            if (!$tasks.length) {
                $tasks = $('<div class="socialtasks"><div></div></div>');
                $('#personabar-panels').after($tasks);
            }

            //hide task bar per CONTENT-8436
            $tasks.hide();
            return;

            // Setting for 1024 resolution
            var width = parent.document.body.clientWidth;
            var socialTasksleft = 941;
            if (width <= 1024 && width > 768) {
                socialTasksleft = 781;
                $tasks.addClass("view-ipad landscape");
            } else if (width <= 768) {
                socialTasksleft = 581;
                $tasks.addClass("view-ipad portrait");
            }

            $tasks.delay(100).animate({ left: socialTasksleft }, 189, 'linear', function () {
                self.loadTemplate('', 'tasks', $('div.socialtasks > div'));
            });
        },
        leave: function() {
            $('.socialtasks').animate({ left: -300 }, 189, 'linear', function () {});
        }
    }
});