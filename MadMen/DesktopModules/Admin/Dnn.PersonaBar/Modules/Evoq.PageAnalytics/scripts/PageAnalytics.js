﻿'use strict';
define(['jquery',
        'main/config',
        'main/loader',
        './analytics/tokenService'
],
function ($, cf, loader, tokenService) {
    var initCallback;
    var utility;
    var settings;
    var config = cf.init();
    var isEnabled = false;

    function loadScript(basePath) {
        var h = window.location.protocol + "//" + window.location.host;
        var path = decodeURIComponent(settings.currentUrl).substring(h.length);
        var url = basePath + "evoqanalytics.js?accountcode=" + settings.accountCode + "&path=" + path;
        $.ajax({
            dataType: "script",
            cache: true,
            url: url
        });
    }

    function getBundleLanguage(culture) {
        var fallbackLanguage = "en";
        var availableLanguages = ["en"];
        return availableLanguages.indexOf(culture) > 0 ? culture : fallbackLanguage;
    }

    function loadOptInScript(basePath) {
        var normalizedCulture = config.culture.split("-")[0];
        var language = getBundleLanguage(normalizedCulture);
        var url = basePath + "/bundle-" + language + ".js";
        $.ajax({
            dataType: "script",
            cache: true,
            url: url
        });
    }

    function initPageAnalytics(util, publicPath) {
        tokenService.init(util.sf);

        window.initPageAnalytics = function initializePageAnalytics() {
            window.dnn.cloudAnalytics.init({
                publicPath: publicPath,
                getToken: tokenService.getToken,
                tokenKey: settings.tokenKey,
                apiServiceUrl: settings.apiUrl,
                libraryVersion: settings.libraryVersion,
                loader: loader,
                notifier: {
                    confirm: util.confirm,
                    notify: util.notify,
                    notifyError: util.notifyError
                }
            });
        }
    }

    function requestFeature(done, fail) {
        var sf = utility.sf;
        sf.moduleRoot = 'AnalyticsLibrary';
        sf.controller = 'OptIn';
        sf.post('RequestAnalytics', {})
            .done(done)
            .fail(fail);
        settings.requested = true;
    }

    function enableFeature(done, fail) {
        var sf = utility.sf;
        sf.moduleRoot = 'AnalyticsLibrary';
        sf.controller = 'OptIn';
        sf.post('Enable', {})
            .done(function () {
                isEnabled = true;
                done();
            })
            .fail(fail);
    }

    function closeModal() {
        window.dnn.stopEscapeFromClosingPB = false;
        if (isEnabled) {
            window.parent && window.parent.location.reload();
        } else {
            utility.closePersonaBar();
        }
    }

    function initOptInServices(publicPath, mode) {
        window.initOptIn = function initializeOptIn() {
            window.dnn.optIn.init({
                publicPath: publicPath,
                mode: mode,
                canEnable: settings.canEnable,
                enableFeature: settings.canEnable ? enableFeature : requestFeature,
                closeModal: closeModal
            });

            if (typeof initCallback === 'function') {
                initCallback();
            }
        }
    }

    function getOptInMode() {
        return settings.requested && !settings.canEnable ? "page-analytics-wait" : "page-analytics";
    }

    return {
        init: function (wrapper, util, params, callback) {
            initCallback = callback;
            utility = util;
            settings = params.settings;

            if (!settings) {
                throw new Error("Analytics settings are not defined in persona bar customSettings");
            }

            if (settings.enabled) {
                var contentPublicPath = settings.uiUrl + "/dist/";
                initPageAnalytics(util, contentPublicPath);
                loadScript(contentPublicPath);
            } else {
                window.dnn.stopEscapeFromClosingPB = true;
                var optInPublicPath = settings.optInUrl + "/dist/";
                var mode = getOptInMode();
                initOptInServices(optInPublicPath, mode);
                loadOptInScript(optInPublicPath);
            }

            if (typeof callback === 'function') {
                callback();
            }
        },

        load: function (params, callback) {
            var analytics = window.dnn.PageAnalytics;
            settings = params.settings;
            if (analytics && analytics.load) {
                analytics.load();
            }
            
            var optin = window.dnn.optIn;
            if (optin && optin.load) {
                if (!settings.enabled) {      
                    var mode = getOptInMode();
                    optin.load(mode);
                } else {
                    $("#PageAnalytics-panel #opt-in-container").empty()
                    var contentPublicPath = settings.uiUrl + "/dist/";
                    initPageAnalytics(utility, contentPublicPath);
                    loadScript(contentPublicPath);
                }
            }
            
            if (typeof callback === 'function') {
                callback();
            }
        }
    };
});
