﻿define(function () {
    'use strict';
    var sf;

    return {
        init: function init(serviceFramework) {
            sf = serviceFramework;
        },
        getLanguagesToken: function getLanguagesToken(onSuccess, onError, forceNewToken) {
            if (!sf) {
                throw new Error('Service Framework is not defined');
            }
            sf.moduleRoot = 'PersonaBar';
            sf.controller = 'Microservices';
            sf.post('GetLanguagesAuthorizationToken', { forceNewToken: forceNewToken })
                .done(function onDone(data) {
                    var token = data.token;
                    onSuccess(token);
                })
                .fail(onError);
        }
    }
});
