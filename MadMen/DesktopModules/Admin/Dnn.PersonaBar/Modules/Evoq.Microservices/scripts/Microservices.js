'use strict';
define(['jquery',
    'main/config',
    'main/loader',
    './tokenService',
    './microservicesService'
],
    function ($, cf, loader, tokenService, microservicesService) {
        var config = cf.init();

        function getBundleLanguage(culture) {
            var fallbackLanguage = "en";
            var availableLanguages = ["en"];
            return availableLanguages.indexOf(culture) > 0 ? culture : fallbackLanguage;
        }

        function loadScript(basePath) {
            var normalizedCulture = config.culture.split("-")[0];
            var language = getBundleLanguage(normalizedCulture);
            var url = basePath + "bundle-" + language + ".js";
            $.ajax({
                dataType: "script",
                cache: true,
                url: url
            });
        }

        function eventNotifier(eventIdentifier) {
            $(document).trigger("personabar:" + eventIdentifier);
        }

        return {
            init: function (wrapper, util, params, callback) {
                var settings = params.settings;
                var publicPath = settings.uiUrl + "/dist/";

                tokenService.init(util.sf);
                microservicesService.init(util);

                window.initMicroservices = function initMicroservices() {
                    window.dnn.Microservices.init({
                        publicPath: publicPath,
                        libraryVersion: settings.libraryVersion,
                        availableFeatures: settings.availableFeatures,
                        enabledFeatures: settings.enabledFeatures,
                        getClientTenantGroupId: microservicesService.getClientTenantGroupId,
                        enableMicroservice: microservicesService.enableMicroservice,
                        eventNotifier: eventNotifier,
                        languages: {
                            getToken: tokenService.getLanguagesToken,
                            tokenKey: settings.languagesTokenKey,
                            apiUrl: settings.languagesApiUrl
                        },
                        loader: loader,
                        utils: util,
                        config: config
                    });

                    if (typeof callback === "function") {
                        callback();
                    }
                };

                loadScript(publicPath);
            },

            load: function (params, callback) {
                if (typeof callback === 'function') {
                    callback();
                }
            }
        };
    });


