﻿define(function () {
    'use strict';
    var sf;
    var util;

    function enableMenuSettings(id) {
        var settings = util.findMenuSettings(id);
        settings.enabled = true;
        util.updateMenuSettings(id, settings);
    }

    return {
        init: function init(utilities) {
            util = utilities;
            sf = util.sf;
            
        },
        getClientTenantGroupId: function getClientTenantGroupId(onSuccess, onError) {
            if (!sf) {
                throw new Error('Service Framework is not defined');
            }
            sf.moduleRoot = 'DNNCorp/EvoqMicroservicesLibrary';
            sf.controller = 'Microservices';
            sf.get('GetClientTenantGroupId')
                .done(onSuccess)
                .fail(onError);
        },
        enableMicroservice: function enableMicroservice(featureName, onSuccess, onError) {
            if (!sf) {
                throw new Error('Service Framework is not defined');
            }
            sf.moduleRoot = 'DNNCorp/EvoqMicroservicesLibrary';
            sf.controller = 'Microservices';
            sf.post('EnableMicroservice', { featureName: featureName })
                .done(function() {
                    switch (featureName) {
                        case "StructuredContent":
                            enableMenuSettings("Evoq.StructuredContent");
                            break;
                        case "FormBuilder":
                            enableMenuSettings("Evoq.Forms");
                            break;
                        case "Analytics":
                            enableMenuSettings("Evoq.PageAnalytics");
                            enableMenuSettings("Evoq.SiteAnalytics");
                            break;
                    }
                    onSuccess();
                }) .fail(onError);
        }
}
});
