﻿define(['jquery', 'main/config', './pages.thumbnails'], function ($, cf, pageThumbnails) {
    'use strict';
    var isMobile;
    var identifier;
    var config = cf.init();

    var init = function (wrapper, util, params, callback) {
        identifier = params.identifier;
        window.dnn.initEvoqPages = function initEvoqPages() {
            return {
                utilities: util,
                moduleName: "EvoqPages",
                config: config,
				pageThumbnails: pageThumbnails
            };
        };

        util.loadBundleScript('modules/dnn.pages/scripts/bundles/evoq-pages-bundle.js');

        window.dnn.pages.setItemTemplate("evoq-pages-list-item-template");
        window.dnn.pages.setDragItemTemplate("evoq-pages-drag-item-template");
        if (window.dnn.pages.pageHierarchyManager &&
            window.dnn.pages.pageHierarchyManager._initialized) {
            window.dnn.pages.pageHierarchyManager._resizeContentContainer(true);
        }
		
		if (typeof callback === 'function') {
			callback(wrapper);
        }
    };

    var initMobile = function (wrapper, util, params, callback) {
        isMobile = true;
        this.init(wrapper, util, params, callback);
    };

    var load = function (params, callback) {
        if (typeof callback === 'function') {
            callback();
        }
    };

    var loadMobile = function (params, callback) {
        isMobile = true;
        this.load(params, callback);
    };

    return {
        init: init,
        load: load,
        initMobile: initMobile,
        loadMobile: loadMobile
    };
});
