﻿// DotNetNuke® - http://www.dnnsoftware.com
//
// Copyright (c) 2002-2018, DNN Corp.
// All rights reserved.

﻿if (typeof dnn === "undefined" || dnn === null) { dnn = {}; };

define(['jquery', 'knockout', 'main/config'], function ($, ko, cf) {
    var config = cf.init();

    var pageThumbnails = function(options) {
        this.options = options;
		$(document.body).on('thumbnailcreated', $.proxy(this._thumbnailCreated, this));
		$(document.body).on('mouseOverThumbnail', $.proxy(this._mouseOverThumbnailHandler, this));
		$(document.body).on('mouseOutThumbnail', $.proxy(this._mouseOutThumbnailHandler, this));	
    };

    var delayTime = 500;	

    pageThumbnails.prototype = {
        constructor: pageThumbnails,

        init: function($panel) {
            this.options = $.extend({}, pageThumbnailsDefaultOptions, this.options);

            this.container = $panel;
            this.wrapper = $('.thumbnails-loader-wrapper');
            this._abort = false;   
        },

        updateThumbnails: function (tabId, tabUrl) {
            if (!this.container.is(':visible')) {
                return;
            }

            var handler = this;
            var defaultName = this.options.defaultThumbnail;

            //if loading img, then ignore
            if (this.container.find('img[src$="' + defaultName + '"][class~="loading"]').length > 0) {
                return;
            }

            var $defaultImg = this.container.find('img[src$="' + defaultName + '"]')
								.filter(':not([class~="loading"])')
								.filter(':not([class~="failed"])')
								.filter(':not([class~="disabled"])').eq(0);

            if ($defaultImg.length == 0) {
                this.removeThumbnailLoader(tabId !== undefined ? tabId : this.container.attr('id').replace('evoq-page-thumbnail-', ''));
                return;
            }
                
            if (typeof tabId === "number") {
                //ignore skin templates
                if (tabId <= 0) {
                    $defaultImg.addClass('failed').parent().addClass('failed');

                    this.updateThumbnails(tabId, tabUrl);
                    return;
                }
                //if external url then doesn't create thumbnail.
                var protocol = tabUrl.indexOf('://');
                var checkUrl = tabUrl.substr(protocol > -1 ? protocol + 3 : 0);
                if (checkUrl.toLowerCase().indexOf(location.host) != 0) {
                    $defaultImg.addClass('failed').parent().addClass('failed');

                    this.updateThumbnails(tabId, tabUrl);
                    return;
                }

                $defaultImg.addClass('loading');
                $defaultImg.parent().addClass('loading');

                var url = tabUrl
                    + (tabUrl.indexOf('?') == -1 ? '?' : '&')
                    + 'dnnprintmode=true&createthumbnail=true&userid=' + config.userId;

                this._abort = false;
                $.get(url, function (data) {
                    if (handler._abort) {
                        return;
                    }
                    handler.removeThumbnailLoader(tabId);

                    var thumbnailLoader = $('<iframe id="thumbnail-loader-' + tabId + '"></iframe>');

                    thumbnailLoader.css({
                        width: $(window).width(),
                        height: $(window).height(),
                        zIndex: -1,
                        visibility: 'hidden',
						position: 'fixed',
						top: 0,
						left: 0
                    });
                    thumbnailLoader.on('load', $.proxy(handler._thumbnailLoaderLoaded, handler));
                    thumbnailLoader.data('listitem', $defaultImg.parent());
                    thumbnailLoader.attr('data-currentid', tabId);

                    handler.wrapper.append(thumbnailLoader);

                    var iframeDoc = thumbnailLoader[0].contentWindow.document;

                    var ie = (function(){

                        var undef,
                            v = 3,
                            div = document.createElement('div'),
                            all = div.getElementsByTagName('i');

                        while (
                            div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
                            all[0]
                        );

                        return v > 4 ? v : undef;

                    }());

                    if (ie && ie < 10) {
                        var wrapper = $('<div></div>');
                        wrapper.html(data);
                        wrapper.find('script').each(function() {
                            if ($(this).html() != '') {
                                $(this).attr('defer', 'defer');
                            }
                        });

                        data = wrapper.html();
                    }

                    var baseUrl = url;
                    if (baseUrl.indexOf('?') > -1) {
                        baseUrl = baseUrl.substr(0, baseUrl.indexOf('?'));
                    }

                    //replace anchor links to full url with base url, and update iframe's location info to base url.
                    //so that can avoid jquery tabs error.
                    data = data.replace(/href=(['"])#(.+?)\1/gi, "href=$1" + baseUrl + "#$2$1");
                    data = data.replace('</head>', '<base href="' + baseUrl + '" /><script type="text/javascript">if(window.history){ window.history.pushState({},"", "' + baseUrl + '");};</script></head>');

                    iframeDoc.write(data);
                    iframeDoc.close();

	                thumbnailLoader.data('contentloaded', true);
                }).fail(
                    function errorCallback(e){
                        $defaultImg.removeClass('loading');
                        $defaultImg.parent().removeClass('loading');
                        $defaultImg.addClass('failed').parent().addClass('failed');

                        setTimeout(function() {
                            handler.updateThumbnails();
                        }, 0);
                    }
                );
            };
        },

        _thumbnailCreated: function (e, data) {
            var $loader = this.wrapper.find('iframe[data-currentid=' + data.pageId + ']');
            var $listItem = $loader.data('listitem');
            if ($listItem) {
                $listItem.find('img.thumbnail')
                    .attr('src', data.thumbnails[2])
                    .removeClass('loading').parent().removeClass('loading');
            } else {
                this.wrapper.find('iframe.thumbnail-loader').data('listitem').find('img.thumbnail')
                    .attr('src', data.thumbnails[2])
                    .removeClass('loading').parent().removeClass('loading');
            }
			
			if (typeof this.options.onThumbnailCreated === "function") {
                this.options.onThumbnailCreated(data);
            }

            var handler = this;
            setTimeout(function() {
                handler.updateThumbnails();
            }, 0);
        },

        _thumbnailLoaderLoaded: function (e) {
            var loader = e.target;
            if (!$(loader).data('contentloaded') || !loader.contentWindow || !loader.contentWindow.document || !loader.contentWindow.document.body.innerHTML) {
                return;
            }

            var dnnPage = true;

            try {
            	var contentWindow = loader.contentWindow;
	            if (typeof contentWindow['dnn'] == "undefined" || typeof contentWindow['WebPageThumbnailGenerator'] == "undefined") {
                    dnnPage = false;
                }
            } catch (e) {
                dnnPage = false;
            }

            if (!dnnPage) {
                var $listItem = $(e.target).data('listitem');
                $listItem.find('.thumbnail img').removeClass('loading').parent().removeClass('loading');
                $listItem.find('.thumbnail img').addClass('failed').parent().addClass('failed');

                this.removeThumbnailLoader();

				var handler = this;
				setTimeout(function() {
					handler.updateThumbnails();
				}, 0);
            }
        },

        removeThumbnailLoader: function (tabId) {
            this.wrapper.find('#thumbnail-loader-' + parseInt(tabId)).each(function () {
                var loader = $(this);
                $.removeData(loader);

                try {
                    var iframeDoc = loader[0].contentWindow.document;
                    iframeDoc.write('');
                    iframeDoc.close();
                } catch (ex) {
                    
                }

                loader.remove();
                loader = null;
            });

            this._abort = true;
        },

        _getVar: function (name) {
            var dnn = window != window.top ? window.top['dnn'] : window['dnn'];
            return dnn.getVar(name);
        },
        

        _mouseOverThumbnailHandler: function (e, pageData) {
            if (this._showPreviewTimeoutHandler) {
                clearTimeout(this._showPreviewTimeoutHandler);
            }

            if (pageData.pageType === "normal") {
                this._showPreviewTimeoutHandler = setTimeout(
                    $.proxy(_showPreview, this, pageData, pageData.element), delayTime);
            }
        },

        _mouseOutThumbnailHandler: function () {
            if (this._showPreviewTimeoutHandler) {
                clearTimeout(this._showPreviewTimeoutHandler);
            }

            _hidePreview(this._previewContainer, this._mouseOnPreview);
        }        
    };
	
	function _showPreview(pageData, element) {
        var handler = this;

        if (!this._previewContainer) {
            this._previewContainer = $('<div class="pages-preview"><img src="" /></div>');
            this._previewContainer.mouseenter(function () {
                handler._mouseOnPreview = true;
            }).mouseleave(function (e) {
                handler._mouseOnPreview = false;
                _hidePreview(this._previewContainer, handler._mouseOnPreview);
            });
            $(document.body).append(this._previewContainer);
        }
        this._previewContainer.find('img').attr('src', $(element).attr('src').replace('medium_', 'large_'));

        _calcPreviewPosition(element, this._previewContainer);
        this._previewContainer.show('fast');
    };

    function _hidePreview(previewContainer, mouseOnPreview) {
        if (previewContainer && !mouseOnPreview) {
            previewContainer.hide('fast');
        }
    };

    function _calcPreviewPosition(element, previewContainer) {
        var pos, $element, previewHeight, elementWidth, elementHeight, windowHeight, offset;

		pos = {};
        $element = $(element);
        previewHeight = previewContainer.outerHeight();
        elementWidth = $element.width();
        elementHeight = $element.height();
        windowHeight = $(window).height();
        offset = $element.offset();
		scrollTop = $(window).scrollTop();

        pos.left = offset.left + elementWidth;
        pos.top = offset.top - scrollTop - previewHeight / 2 + elementHeight / 2;

        if (pos.top < 0) {
            previewContainer.removeClass('bottom').addClass('top');
            pos.top = $element.offset().top - scrollTop;
        }
		else if (pos.top + previewHeight > windowHeight) {
            previewContainer.removeClass('top').addClass('bottom');
            pos.top = offset.top - scrollTop - previewHeight + elementHeight;
        } 
		else {
            previewContainer.removeClass('top bottom');
        }

        previewContainer.css({
            left: pos.left,
            top: pos.top
        });
    };

    var pageThumbnailsDefaultOptions = {
        delayTime: 500,
        requestDelayTime: 2000,
        requestTimeout: 4000,
        defaultThumbnail: 'fallback-thumbnail.png'
    };

    return pageThumbnails;
});