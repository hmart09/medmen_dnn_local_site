﻿// DotNetNuke® - http://www.dnnsoftware.com
//
// Copyright (c) 2002-2018, DNN Corp.
// All rights reserved.
'use strict';
define(['jquery', 'main/config'], function ($, cf) {
    var config = cf.init();

    var init = function (wrapper, util, params, callback) {

        window.dnn.initSiteSettings = function initializeSiteSettings() {
            return {
                utility: util,
                siteRoot: config.siteRoot,
                settings: params.settings,
                moduleName: 'EvoqSiteSettings',
                identifier: params.identifier
            };
        };

        window.dnn.SiteSettings = {};

        util.loadBundleScript('modules/dnn.sitesettings/scripts/bundles/evoq-site-settings-bundle.js');

        wrapper.on('init.extension', function () {
            if (!window.dnn.SiteSettings.bundleLoaded) {
                setTimeout(function () {
                    util.loadBundleScript('modules/dnn.sitesettings/scripts/bundles/site-settings-bundle.js');
                }, 0);
            }
        });

        if (typeof callback === "function") {
            callback();
        }
    };

    var initMobile = function (wrapper, util, params, callback) {
        if (typeof callback === "function") {
            callback();
        }
    };

    var load = function (params, callback) {
        if (typeof callback === "function") {
            callback();
        }
    }

    var loadMobile = function (params, callback) {
        if (typeof callback === "function") {
            callback();
        }
    }

    return {
        init: init,
        initMobile: initMobile,
        load: load,
        loadMobile: loadMobile
    };
});