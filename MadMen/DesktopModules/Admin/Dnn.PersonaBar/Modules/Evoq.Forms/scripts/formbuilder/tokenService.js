﻿define(function () {
    'use strict';
    var sf;

    return {
        init: function init(serviceFramework) {
            sf = serviceFramework;
        },
        getToken: function getToken(done, fail, forceNewToken) {
            if (!sf) {
                throw new Error('Service Framework is not defined');
            }
            sf.moduleRoot = 'formbuilderlibrary';
            sf.controller = 'authorization';
            sf.post('GetAuthorizationToken', { forceNewToken: forceNewToken })
                .done(function onSuccess(data) {
                    var token = data.token;
                    done(token);
                })
                .fail(fail);
        }
    }
});