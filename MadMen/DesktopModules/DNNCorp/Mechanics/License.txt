﻿<div class="License">
	<h3>License</h3>
	<p class="Owner">
		DotNetNuke&reg;  <a href=http://www.dnnsoftware.com>http://www.dnnsoftware.com</a></br>
		Copyright (c) 2002-2018</br>
		by DNN Corporation</br>
	</p>
	<p>
		All Rights Reserved
	</p>
</div>