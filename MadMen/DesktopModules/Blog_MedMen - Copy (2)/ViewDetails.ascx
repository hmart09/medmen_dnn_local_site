﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewDetails.ascx.cs" Inherits="Haven.Modules.Blog_MedMen.Components.ViewDetails" %>


<asp:FormView ID="FormView1" runat="server" DataKeyNames="ItemId" DataSourceID="SqlDataSource1">
    <EditItemTemplate>
        ItemId:
        <asp:DynamicControl ID="ItemIdDynamicControl" runat="server" DataField="ItemId" Mode="ReadOnly" />
        <br />
        Title:
        <asp:DynamicControl ID="TitleDynamicControl" runat="server" DataField="Title" Mode="Edit" />
        <br />
        Article:
        <asp:DynamicControl ID="ArticleDynamicControl" runat="server" DataField="Article" Mode="Edit" />
        <br />
        Date Created:
        <asp:DynamicControl ID="Date_CreatedDynamicControl" runat="server" DataField="Date Created" Mode="Edit" />
        <br />
        Created By:
        <asp:DynamicControl ID="Created_ByDynamicControl" runat="server" DataField="Created By" Mode="Edit" />
        <br />
        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" ValidationGroup="Insert" />
        &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
    </EditItemTemplate>
    <InsertItemTemplate>
        Title:
        <asp:DynamicControl ID="TitleDynamicControl" runat="server" DataField="Title" Mode="Insert" ValidationGroup="Insert" />
        <br />
        Article:
        <asp:DynamicControl ID="ArticleDynamicControl" runat="server" DataField="Article" Mode="Insert" ValidationGroup="Insert" />
        <br />
        Date Created:
        <asp:DynamicControl ID="Date_CreatedDynamicControl" runat="server" DataField="Date Created" Mode="Insert" ValidationGroup="Insert" />
        <br />
        Created By:
        <asp:DynamicControl ID="Created_ByDynamicControl" runat="server" DataField="Created By" Mode="Insert" ValidationGroup="Insert" />
        <br />
        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" ValidationGroup="Insert" />
        &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
    </InsertItemTemplate>
    <ItemTemplate>
        ItemId:
        <asp:DynamicControl ID="ItemIdDynamicControl" runat="server" DataField="ItemId" Mode="ReadOnly" />
        <br />
        Title:
        <asp:DynamicControl ID="TitleDynamicControl" runat="server" DataField="Title" Mode="ReadOnly" />
        <br />
        Article:
        <asp:DynamicControl ID="ArticleDynamicControl" runat="server" DataField="Article" Mode="ReadOnly" />
        <br />
        Date Created:
        <asp:DynamicControl ID="Date_CreatedDynamicControl" runat="server" DataField="Date Created" Mode="ReadOnly" />
        <br />
        Created By:
        <asp:DynamicControl ID="Created_ByDynamicControl" runat="server" DataField="Created By" Mode="ReadOnly" />
        <br />

    </ItemTemplate>
</asp:FormView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SiteSqlServer %>" SelectCommand="SELECT Blog_MedMen_Items.ItemId, Blog_MedMen_Items.ItemName AS Title, Blog_MedMen_Items.ItemDescription AS Article, Blog_MedMen_Items.CreatedOnDate AS [Date Created], Users.FirstName + Users.LastName AS [Created By] FROM Blog_MedMen_Items INNER JOIN Users ON Blog_MedMen_Items.CreatedByUserId = Users.UserID WHERE (Blog_MedMen_Items.ItemId = @ItemId)">
    <SelectParameters>
        <asp:QueryStringParameter Name="ItemId" QueryStringField="id" />
    </SelectParameters>
</asp:SqlDataSource>

