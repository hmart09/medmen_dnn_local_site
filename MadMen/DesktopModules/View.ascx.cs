﻿/*
' Copyright (c) 2018  HavenAgency.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.UI.WebControls;
using Haven.Modules.Blog_MedMen.Components;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Haven.Modules.Blog_MedMen
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from Blog_MedMenModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : Blog_MedMenModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //********************************************************* added for debug***********************************************************************
            // System.Diagnostics.Debugger.Launch();

            try
            {
                ItemController tc = new ItemController();
                // rptItemList.DataSource = tc.GetItems(ModuleId);


                // Collection of Items
                IEnumerable<Item> items = tc.GetItems(ModuleId);

                List<Item> itemList = items.ToList();

                foreach (var item in itemList)
                {

                    string str = item.ItemDescription;

                    string cleanStr = StripHTML(str);

                    item.ItemDescription = cleanStr;

                }

                rptItemList.DataSource = itemList;

                rptItemList.DataBind();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        protected void rptItemListOnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HyperLink lnkEdit = e.Item.FindControl("lnkEdit") as HyperLink;
                LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

                Panel pnlAdminControls = e.Item.FindControl("pnlAdmin") as Panel;

                Item t = (Item)e.Item.DataItem;


                if (IsEditable && lnkDelete != null && lnkEdit != null && pnlAdminControls != null)
                {
                    pnlAdminControls.Visible = true;
                    lnkDelete.CommandArgument = t.ItemId.ToString();
                    lnkDelete.Enabled = lnkDelete.Visible = lnkEdit.Enabled = lnkEdit.Visible = true;

                    lnkEdit.NavigateUrl = EditUrl(string.Empty, string.Empty, "Edit", "tid=" + t.ItemId);

                    ClientAPI.AddButtonConfirm(lnkDelete, Localization.GetString("ConfirmDelete", LocalResourceFile));
                }
                else
                {
                    pnlAdminControls.Visible = false;
                }
            }
        }

        public void rptItemListPreRender(object source, RepeaterItemEventArgs e)
        {
            foreach (RepeaterItem item in rptItemList.Items)
            {

                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    string str = String.Empty;

                    Label lbl = (Label)item.FindControl("rptItemList");

                    str = lbl.Text;

                    lbl.Text = "THIS IS A TEST" + str + "THIS ENDS THE TEST";


                }

            }
            


        }

        public void rptItemListOnItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                Response.Redirect(EditUrl(string.Empty, string.Empty, "Edit", "tid=" + e.CommandArgument));
            }


            if (e.CommandName == "Click")
            {
                //Response.Redirect("~/DesktopModules/Blog_MedMen/ViewDetails.ascx?id=" + e.CommandArgument.ToString());
                //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());




            }

            if (e.CommandName == "Delete")
            {
                ItemController tc = new ItemController();
                tc.DeleteItem(Convert.ToInt32(e.CommandArgument), ModuleId);
            }
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                ModuleActionCollection actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        public string LinkToPage(int tabId, string param1, string param2, string param3, string param4, string param5, string param6)
        {
            string result = DotNetNuke.Common.Globals.NavigateURL(tabId, "", new string[] { param1, param2, param3, param4, param5, param6 });
            return result;
        }

        public string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public interface IEnumerable
        {

            IEnumerator GetEnumerator();

        }

    }
}