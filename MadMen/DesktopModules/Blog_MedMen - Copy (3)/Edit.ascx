﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Edit.ascx.cs" Inherits="Haven.Modules.Blog_MedMen.Edit" %>
<%@ Register TagPrefix="dnn" TagName="label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx" %>
<%@ Register TagPrefix="dnn" TagName="filepickeruploader" Src="~/controls/filepickeruploader.ascx" %>

<div  id="dnnEditBasicSettings">

    <h2 id="dnnSitePanel-BasicSettings">
        <a href="" class="dnnSectionExpanded">
            <%=LocalizeString("BasicSettings")%></a></h2>
    
    <div>
    
    <fieldset class= "">


        <div class="dnnFormItem">
            <dnn:Label ID="lblImage" runat="server" ControlName="ctlImage"  Suffix=":"/>
            <dnn:FilePickerUploader ID="ctlImage" runat="server" Required="True" />
        </div>

        
        <div class="dnnFormItem">
            <dnn:label ID="lblName" runat="server" />
            <asp:TextBox ID="txtName" runat="server" />
        </div>
        <div class="auto-style1">

            <%--<dnn:label ID="lblDescription" runat="server" />--%>
            <%--<asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="5" Columns="20" />--%>

            <dnn:TextEditor ID="txtDescription" runat="server" Height="500px" Width="100%" />

        </div>
        <div class="dnnFormItem">
            <dnn:label ID="lblAssignedUser" runat="server" />
            <asp:DropDownList ID="ddlAssignedUser" runat="server" />
        </div>

    </fieldset>
    
    </div>
    
</div>
<div class="auto-style1">
<asp:LinkButton ID="btnSubmit" runat="server"
    OnClick="btnSubmit_Click" resourcekey="btnSubmit" CssClass="dnnPrimaryAction" />
<asp:LinkButton ID="btnCancel" runat="server"
    OnClick="btnCancel_Click" resourcekey="btnCancel" CssClass="dnnSecondaryAction" />

</div>


<script type="text/javascript">
    /*globals jQuery, window, Sys */
    (function ($, Sys) {
        function dnnEditBasicSettings() {
            $('#dnnEditBasicSettings').dnnPanels();
            $('#dnnEditBasicSettings .dnnFormExpandContent a').dnnExpandAll({ expandText: '<%=Localization.GetString("ExpandAll", LocalResourceFile)%>', collapseText: '<%=Localization.GetString("CollapseAll", LocalResourceFile)%>', targetArea: '#dnnEditBasicSettings' });
        }

        $(document).ready(function () {
            dnnEditBasicSettings();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                dnnEditBasicSettings();
            });
        });

    }(jQuery, window.Sys));
</script>
