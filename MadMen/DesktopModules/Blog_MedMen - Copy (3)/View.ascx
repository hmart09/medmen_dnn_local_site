﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Haven.Modules.Blog_MedMen.View" %>
<asp:Repeater ID="rptItemList" runat="server" OnItemDataBound="rptItemListOnItemDataBound" OnItemCommand="rptItemListOnItemCommand" >
    <HeaderTemplate>
        <ul class="tm_tl">
    </HeaderTemplate>

    <ItemTemplate>
        <li class="tm_t" property="og:determiner">
            <asp:Image runat="server" ID="itemImage"/>
            <h3>
                <asp:Label ID="lblitemName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ItemName").ToString() %>' />
            </h3>
            <asp:Label ID="lblItemDescription" runat="server" Text='<%#Server.HtmlDecode(DataBinder.Eval(Container.DataItem,"ItemDescription").ToString()) %>' CssClass="tm_td" />
            <br /><br />
            <div>

                <asp:Button ID="btn" CommandName="Click" Text="Read More & Comment" runat="server" CommandArgument='<%# Eval("ItemId") %>' />

            </div>



            <asp:Panel ID="pnlAdmin" runat="server" Visible="false">
                <asp:HyperLink ID="lnkEdit" runat="server" ResourceKey="EditItem.Text" Visible="false" Enabled="false" />
                <asp:LinkButton ID="lnkDelete" runat="server" ResourceKey="DeleteItem.Text" Visible="false" Enabled="false" CommandName="Delete" />
            </asp:Panel>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
