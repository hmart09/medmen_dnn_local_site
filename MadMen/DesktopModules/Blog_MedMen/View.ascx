﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Haven.Modules.Blog_MedMen.View" %>
<link href="module.css" rel="stylesheet" type="text/css" />
<asp:Repeater ID="rptItemList" runat="server" OnItemDataBound="rptItemListOnItemDataBound" OnItemCommand="rptItemListOnItemCommand">
    <HeaderTemplate>
    </HeaderTemplate>

    <ItemTemplate>
        <div class="container">

            <div class="top-left">

                <h3>
                    <asp:Label ID="lblitemName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ItemName").ToString() %>' />
                </h3>

            </div>

            <%--image goes here--%>
            <asp:Image runat="server" ID="itemImage" CssClass="image" />



            <div class="bottom-right">

                <asp:Label ID="lblItemDescription" runat="server" Text='<%#Server.HtmlDecode(DataBinder.Eval(Container.DataItem,"ItemDescription").ToString()) %>' />

            </div>


            <div class="bottom-left">

                <asp:Panel ID="pnlAdmin" runat="server" Visible="false">
                    <asp:HyperLink ID="lnkEdit" runat="server" ResourceKey="EditItem.Text" Visible="false" Enabled="false" />
                    <asp:LinkButton ID="lnkDelete" runat="server" ResourceKey="DeleteItem.Text" Visible="false" Enabled="false" CommandName="Delete" />
                </asp:Panel>

            </div>



        </div>







    </ItemTemplate>
    <FooterTemplate>
    </FooterTemplate>
</asp:Repeater>
